package com.example.hello10;




import java.util.List;



import android.app.Activity;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;



public class MainActivity extends Activity
{
	//public static myPhoneStateListener psListener ;
	public TelephonyManager telephonyManager;
	//public static TelephonyManager telephonyManager1;
	
	public TextView txtSignalStr;
	public TextView GSMid;
	public static TextView n1;
	public static TextView n2;
	public static final String TAG = "TelephonyDemoActivity";
	myPhoneStateListener plistener;
	WifiManager wifiManager;
	int level =0;
	@Override
    protected void onCreate(final Bundle savedInstanceState)
    {
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
   
      	 /* if (wifiManager.isWifiEnabled() == false)
            {
      		 Toast.makeText(getApplicationContext(), "wifi is disabled..making it enabled", Toast.LENGTH_LONG).show();
                wifiManager.setWifiEnabled(true);
            }  
      
      // Level of a Scan Result
      List<ScanResult> wifiList = wifiManager.getScanResults();
      for (ScanResult scanResult : wifiList) {
       level = WifiManager.calculateSignalLevel(scanResult.level, 5);
      }
       int rssi = wifiManager.getConnectionInfo().getRssi();
      int level = WifiManager.calculateSignalLevel(rssi, 5);
      String s= wifiManager.getConnectionInfo().getSSID();


      String lis="";
      for (ScanResult result : wifiList) {
        lis +=  "name: "+result.SSID +", strength :"+result.level+"\n";
      }*/
       //////////
        plistener= new 	myPhoneStateListener();
        txtSignalStr = (TextView)findViewById(R.id.signalStrength);
        GSMid  = (TextView)findViewById(R.id.wifi);
        n1  = (TextView)findViewById(R.id.nn);
        n2  = (TextView)findViewById(R.id.mm);
    
        telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        

        telephonyManager.listen(plistener,
                PhoneStateListener.LISTEN_SIGNAL_STRENGTHS  | PhoneStateListener.LISTEN_CELL_LOCATION | PhoneStateListener.LISTEN_CELL_INFO);

        Context context=getApplicationContext();
     	 wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
       

        ////////////

      //  psListener = new myPhoneStateListener();//define a listener for monitering changes
        //////////////////
    
 
    
        
        
      //  n1.setText(myPhoneStateListener.telephonyInfo.toString());
     //  txtSignalStr.setText("Signal Strength : " + myPhoneStateListener.signalStrengthValue);
     //  GSMid.setText("id + loc : " + myPhoneStateListener.str + myPhoneStateListener.cellId + myPhoneStateListener.cellLac);
        ///////////////
     
      /* 
        telephonyManager = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        //access to information about the telephony services on the device
        telephonyManager.listen(psListener,PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        // Listener for the signal strength.

        telephonyManager = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(psListener, PhoneStateListener.LISTEN_CELL_LOCATION);
     
	   */
    ////////////
        
        /* first you wanna get a telephony manager by asking the system service */
     // n1  = (TextView)findViewById(R.id.nn);
     //   TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        /* then you can query for all the neighborhood cells */
    //    List<NeighboringCellInfo> neighbors = tm.getNeighboringCellInfo();

        /* here's something you can get from NeighboringCellInfo */
     //  for (NeighboringCellInfo n : neighbors) {
        //    Log.v("CellInfo", "" + n.getCid());
         //   Log.v("CellInfo", "" + n.getLac());
          //  Log.v("CellInfo", "" + n.getPsc());
           // Log.v("CellInfo", "" + n.getRssi());
        //    n1.setText( Log.v("CellInfo", "" + n.getCid()));
       // }
    /////////////////////////
        
  
      
         

        
        // TelephonyManager mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
///////////////if (mTelephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
/*
		if (telephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
		GsmCellLocation location = (GsmCellLocation) telephonyManager
				.getCellLocation();
		 
		 

		Log.i(TAG, "Current Location LAC:" + location.getLac() + ", CID:" + location.getCid());

		telephonyInfo.append("Current Location Area Code: " + location.getLac()
				+ "\r\n");
		telephonyInfo.append("Current Location Cell ID: " + location.getCid()
				+ "\r\n");
	} else
		telephonyInfo.append("It's not a GSM phone!\r\n");
        */
        //////////////////
  
	}

	public class myPhoneStateListener extends PhoneStateListener{
	
	 public int signalStrengthValue;
	
   // Context mContext;
  //  public myPhoneStateListener (Context context){
    // mContext = context;
   //  }

   // @TargetApi(Build.VERSION_CODES.GINGERBREAD
    @Override
    public void onCellLocationChanged (CellLocation location) {
    	super.onCellLocationChanged(location);
    	
        
        
        StringBuffer str = new StringBuffer();
        // GSM
       location = telephonyManager.getCellLocation();
       if (location == null) {
           GSMid.setText("no cell");
        }
       // GsmCellLocation loc = (GsmCellLocation) telephonyManager.getCellLocation();
           
        if (location instanceof GsmCellLocation) {
 	       GsmCellLocation loc = (GsmCellLocation) location;

        
            str.append("gsm ");
            str.append(loc.getCid() & 0xffff);
            str.append(" ");
            str.append(loc.getLac() & 0xffff);
            Log.d(TAG, str.toString());
            String cellId = Integer.toHexString(loc.getCid());
            String cellLac = Integer.toHexString(loc.getLac());
            GSMid.setText("id + loc : " + str + cellId + cellLac);
            }
        else if (location instanceof CdmaCellLocation){
        	 CdmaCellLocation cdma = (CdmaCellLocation) location;

	            str.append("gsm ");
	            str.append(cdma.getBaseStationId() & 0xffff);
	            str.append(" ");
	            str.append(cdma.getNetworkId() & 0xffff);
	            Log.d(TAG, str.toString());
	            String cellId = Integer.toHexString(cdma.getBaseStationId());
	            String cellLac = Integer.toHexString(cdma.getNetworkId());
	            GSMid.setText("id + loc : " + str + cellId + cellLac);
        }
        
        n1  = (TextView)findViewById(R.id.nn);
        StringBuilder telephonyInfo = new StringBuilder();
        // TelephonyManager mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

     	List<NeighboringCellInfo> mNeighboringCellInfo = telephonyManager.getNeighboringCellInfo();

		if (!mNeighboringCellInfo.isEmpty()) {
			int i = 1;
			for (NeighboringCellInfo cellInfo : mNeighboringCellInfo) {
				Log.i(TAG,
						"Neighbor CellInfo No." + i + " LAC:"
								+ cellInfo.getLac() + ", CID:"
								+ cellInfo.getCid() + ", RSSI:"
								+ cellInfo.getRssi());
				i++;
				telephonyInfo.append("Neighbor CellInfo No." + i
						+ ", Location Area Code: " + (cellInfo.getLac()& 0xffff)
						+ ", Cell ID: " + (cellInfo.getCid()& 0xffff)
						+ ", Signal Strength: " + (cellInfo.getRssi() *2-113)+ "\r\n");
			}
		} else
			telephonyInfo.append("No Neighbor Cell Info!");

		n1.setText(telephonyInfo.toString());
		  ///////////////////////
		 //// Setup WiFi
		  	
		 // 	List<ScanResult> wireless = myWifiManager.getScanResults(); // Returns a <list> of scanResults
		    String lis="danya";
		  	if(wifiManager.isWifiEnabled()){
		  		if(wifiManager.startScan()){
		  		// List available APs
		  		List<ScanResult> scans = wifiManager.getScanResults();
		  		if(scans != null && !scans.isEmpty()){
		  		for (ScanResult scan : scans) {
		  		int level = WifiManager.calculateSignalLevel(scan.level, 20);
		  		//Other code
		  		}
		  		int rssi = wifiManager.getConnectionInfo().getRssi();
		        int level = WifiManager.calculateSignalLevel(rssi, 5);
		        String s= wifiManager.getConnectionInfo().getSSID();
		      
		        for (ScanResult result : scans) {
		          lis +=  "name: "+result.SSID +", strength :"+result.level+"\n";
		          
		        }
		  		}
		  		}
		  		}
		  	 n2.setText(lis);
		  	///////////////////
    }
    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        super.onSignalStrengthsChanged(signalStrength);
     
        onCellLocationChanged(telephonyManager.getCellLocation());
        
     //   main.refreshStationInfo();
        if (signalStrength.isGsm()) // true if this is for GSM
        {   
            if (signalStrength.getGsmSignalStrength() != 99)
                signalStrengthValue = signalStrength.getGsmSignalStrength() * 2 - 113;//Get the GSM Signal Strength, valid values are (0-31, 99) 
            //returns the ASU (Active State Update) value, RSSI = -113 + 2 * ASU
         //   0        -113 dBm or less  
          //  1        -111 dBm  
          //  2...30   -109... -53 dBm  
          //  31        -51 dBm or greater  

          //  99 not known or not detectable
            else
                signalStrengthValue = signalStrength.getGsmSignalStrength();
        } else {
            signalStrengthValue = signalStrength.getCdmaDbm();//Get the CDMA RSSI value in dBm
        }
        
        
        n1  = (TextView)findViewById(R.id.nn);
        StringBuilder telephonyInfo = new StringBuilder();
        // TelephonyManager mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

     	List<NeighboringCellInfo> mNeighboringCellInfo = telephonyManager.getNeighboringCellInfo();

		if (!mNeighboringCellInfo.isEmpty()) {
			int i = 1;
			for (NeighboringCellInfo cellInfo : mNeighboringCellInfo) {
				Log.i(TAG,
						"Neighbor CellInfo No." + i + " LAC:"
								+ cellInfo.getLac() + ", CID:"
								+ cellInfo.getCid() + ", RSSI:"
								+ cellInfo.getRssi());
				i++;
				telephonyInfo.append("Neighbor CellInfo No." + i
						+ ", Location Area Code: " + (cellInfo.getLac()& 0xffff)
						+ ", Cell ID: " + (cellInfo.getCid()& 0xffff)
						+ ", Signal Strength: " + (cellInfo.getRssi() *2-113)+ "\r\n");
			}
		} else
			telephonyInfo.append("No Neighbor Cell Info!");

		n1.setText(telephonyInfo.toString());
	
    txtSignalStr.setText("Signal Strength : " + signalStrengthValue);
  
   
    ///////////////////////
 //// Setup WiFi
  	
 // 	List<ScanResult> wireless = myWifiManager.getScanResults(); // Returns a <list> of scanResults
    String lis="danya";
  	if(wifiManager.isWifiEnabled()){
  		if(wifiManager.startScan()){
  		// List available APs
  		List<ScanResult> scans = wifiManager.getScanResults();
  		if(scans != null && !scans.isEmpty()){
  		for (ScanResult scan : scans) {
  		int level = WifiManager.calculateSignalLevel(scan.level, 20);
  		//Other code
  		}
  		int rssi = wifiManager.getConnectionInfo().getRssi();
        int level = WifiManager.calculateSignalLevel(rssi, 5);
        String s= wifiManager.getConnectionInfo().getSSID();
      
        for (ScanResult result : scans) {
          lis +=  "name: "+result.SSID +", strength :"+result.level+"\n";
          
        }
  		}
  		}
  		}
  	 n2.setText(lis);
  	///////////////////
    }
/*
    @SuppressWarnings("deprecation")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onCellInfoChanged(List<CellInfo> cellInfo) {
    super.onCellInfoChanged(cellInfo);
    StringBuilder telephonyInfo = new StringBuilder();
   	List<NeighboringCellInfo> mNeighboringCellInfo;
		mNeighboringCellInfo = telephonyManager.getNeighboringCellInfo();

		if (!mNeighboringCellInfo.isEmpty()) {
			int i = 1;
			for (NeighboringCellInfo cellInfo1 : mNeighboringCellInfo) {
				Log.i(TAG,
						"Neighbor CellInfo No." + i + " LAC:"
								+ cellInfo1.getLac() + ", CID:"
								+ cellInfo1.getCid() + ", RSSI:"
								+ cellInfo1.getRssi());
				i++;
				telephonyInfo.append("Neighbor CellInfo No." + i
						+ ", Location Area Code: " + cellInfo1.getLac()
						+ ", Cell ID: " + cellInfo1.getCid()
						+ ", Signal Strength: " + cellInfo1.getRssi() + "\r\n");
			}
		} else
			telephonyInfo.append("No Neighbor Cell Info!");

		n1.setText(telephonyInfo.toString());

  //  Log.i(main.TAG, "onCellInfoChanged: " + cellInfo);
    }
    */
}


		//public static MainActivity main;
	
	


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



}

